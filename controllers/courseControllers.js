const User = require("../models/User.js")
const Course = require("../models/Course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")


/*
	import the necessary models that you will need to perform CRUD operations

	1. Find the user in the database 
		Find out if the user is admin

	2. if the user is not an admin, send the response "You are not an admin"

	3. if the user is an admin, create the new COurse object
		save the object
			if there are errors, return false
			if the are no errors , return "Course created successfully"

*/

module.exports.addCourse = (requestBody, userData) => {
	return User.findOne({ email: userData.email, isAdmin: userData.isAdmin}).then(result =>{
		if(userData.isAdmin === false){
			return "You are not an admin"
		}else {
			let newCourse = new Course ({
				name: requestBody.name,
				description: requestBody.description,
				price: requestBody.price
			})
			return newCourse.save().then((saved, error) => {
				if(error){
					console.log(error)
					return false
				}else{
					return true
				}
			})
		}
	})
}


// retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	})
}