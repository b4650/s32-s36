const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseControllers.js")


router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
})


module.exports = router;

/*
Create a route that will let an admin performa addCourse function in the courseController
	verify that the user is logged in
	decode the token for that user
	use the id, isAdmin, and request body to perform the function in courseController
		id and isAdmin are parts of an object inside the token
*/

/*
	Create a route that will revtrieve all of our products/courses
		will require login/register functions
*/

router.get("/", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})